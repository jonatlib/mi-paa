#!/bin/bash

declare -x CMD='./knapgen.out'
declare -x NUM_THINGS='20'
declare -x INSTANCES='5'
declare -x BASE='./problems'
declare -x GATHER_DIR="./gather"
declare -x LIMIT=5

function generate() {
  local RATION=$1
  local GRANULARITY=$2
  local MAX_WEIGHT=$3
  local MAX_PRICE=$4
  local DIR=$5

  local DIRNAME="$BASE/$DIR"     
  local FILE="$DIRNAME/instance"

  local COMMAND="$CMD -I 9000 -n $NUM_THINGS -N $INSTANCES -m $RATION -W $MAX_WEIGHT -C $MAX_PRICE -k $GRANULARITY -d 0"

  local OUT=$( $COMMAND 2>/dev/null ) 
  
  mkdir -p $DIRNAME 
  echo -e "$OUT" > $FILE
  echo $FILE
}

function run_solver(){
  local JAR_CMD="./../jar/run.jar"
  local DATAFILE=$1
  local LIMIT=$2
  
  local DIR=$(dirname $DATAFILE)
  LOG_FILE="$DIR/log"

  local COMMAND="$JAR_CMD $DATAFILE - 1 $LIMIT"
    
  timeout 60s java -jar $COMMAND | tee $LOG_FILE
  if [[ $? -eq 124 ]];
  then
    touch "$LOG_FILE.error"
  fi
}

function gather(){
  local LOG_FILE=$1
  local NAME=$2
  local OUT_DIR=$GATHER_DIR
  
  local DATA=$( cat $LOG_FILE | grep -E "^##(.*)##$" | sed s/#//g | sed s/^Ethalon.*$//g | sed "s/^[^H]*ERROR|0.*//g" | sed s/^ExpanseOverWeight\|ERROR.*$//g | grep -v "^$" )

  local RESULT="##$NAME##\n\n$DATA"

  mkdir -p $OUT_DIR
  echo -e "$RESULT" > "$OUT_DIR/$NAME"
}

function run(){
  local DIR=$5
  local LIMIT=$LIMIT

  INSTANCE=$(generate "$1" "$2" "$3" "$4" "$5")
  run_solver $INSTANCE $LIMIT
  gather $LOG_FILE $DIR
}

echo "Weight: 100, Price: 200, Ration: 0.5, Granularity: 1"

echo "Max weight..."
for i in {30..300..10};
do
  NAME="weight-$i"
  VARIABLE="$i"
  run 0.5 1 $VARIABLE 200 $NAME
done;

echo "Max price..."
for i in {10..300..30};
do
  NAME="price-$i"
  VARIABLE="$i"
  run 0.5 1 100 $VARIABLE $NAME
done;

echo "Ration..."
for i in {1..9};
do
  NAME="ration-$i"
  VARIABLE="0.$i"
  run $VARIABLE 1 100 200 $NAME
done;

echo "Granularity..."
for i in {10..55..5};
do
  NAME="granularity-$i"
  VARIABLE=$( echo "scale=1; $i / 10" | bc )
  run 0.5 $VARIABLE 100 200 $NAME
done;

###########################################
#run 0.5 1 21 1 price

rm -rf whole_gather
for i in gather/*;
do
  cat $i >> whole_gather
done;

