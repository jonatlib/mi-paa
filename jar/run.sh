#/bin/bash


NUM=$1
PROBLEM_FILE="../data/inst/knap_$NUM.inst.dat"
SOLUTION_FILE="../data/sol/knap_$NUM.sol.dat"

java -jar run.jar $PROBLEM_FILE $SOLUTION_FILE $2 $3 $4

