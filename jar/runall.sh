#!/bin/bash

DIR="../data/inst/"
TYPE=$1
NUMBERS=""

for i in `ls $DIR`
do
  IN=`echo $i | cut -d"_" -f2 | cut -d"." -f1`;
  NUMBERS="$NUMBERS\n$IN";
done;

NUMBERS=`echo -e $NUMBERS | sort -n`
for i in $NUMBERS
do
   echo "Running test number $i...."
   ./run.sh $i $2 $3 $4 | tee ./log/log-$TYPE-$i.log &
done;

