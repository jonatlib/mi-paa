package cz.jonat.mi_paa.bag.formater;

import cz.jonat.mi_paa.bag.task.SolveProblems;

public class DiffSolverProblemsFormater {

	protected static double getRelativeError(int real, int err){
		return ((double) err / real) * 100.0;
	}
	
	public static String format(SolveProblems best, SolveProblems tested){
		String a = "";
		a += "--------------------------------------------\n";
		a += "As best ethalon: '" + best.getSolver().getName() + "' as subject: '" + tested.getSolver().getName() + "'\n";
		a += "Ethalon found " + best.getSolutions() + " solutions tested found " + tested.getSolutions() + "\n";
		a += "Ethalon found " + best.getBestSolutions() + " best solutions tested found " + tested.getBestSolutions() + "\n";
		a += "Tested solver relative error to problem best solution [%]:" + DiffSolverProblemsFormater.getRelativeError(best.getProblemBestCost(), tested.getError()) + "\n";
		a += "Tested solver relative error to ethalon best solution [%]:" + DiffSolverProblemsFormater.getRelativeError(best.getSolverBestCost(), tested.getError()) + "\n";
		a += "--------Computer readable data--------------\n";
		a += "##" + tested.getSolver().getName() + "|ERROR|" + DiffSolverProblemsFormater.getRelativeError(best.getProblemBestCost(), tested.getError()) + "##\n";
		a += "--------------------------------------------\n\n";
		
		return a;
	}
	
}
