package cz.jonat.mi_paa.bag.formater;

import cz.jonat.mi_paa.bag.watch.RunnableWatch;

public class RunnableWatchFormater {
	
	public static String format(String text, RunnableWatch watch){
		String a = "";
		a += "--------------------------------------------\n";
		a += text + " [us]: " + watch.getTimeInUsec() + "\n";
		a += "--------------------------------------------\n\n";
		
		return a;
	}
	
}
