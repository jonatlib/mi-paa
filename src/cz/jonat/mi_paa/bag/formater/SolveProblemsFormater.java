package cz.jonat.mi_paa.bag.formater;

import cz.jonat.mi_paa.bag.task.SolveProblems;

public class SolveProblemsFormater {

	protected static double getTime(double time, int solutions){
		return (time / (double) solutions);
	}
	
	protected static double getError(int error, int count){
		return (double) error / (double) count;
	}
	
	protected static long statesPerProblem(long states, int count){
		return states / count;
	}
	
	public static String format(SolveProblems solver){
		String a = "";
		a += "--------------------------------------------\n";
		a += "Solving using: " + solver.getSolver().getName() + "\n";
		a += "Solver description: " + solver.getSolver().getDescription() + "\n";
		a += "Solved: " + solver.getCount() + " out of " + solver.getData().size() + "\n";
		a += "BestSolutions found: " + solver.getBestSolutions() + "\n";
		a += "Solutions found: " + solver.getSolutions() + "\n";
		a += "Solved in [us]: " + solver.getTimeInUs() + "\n";
		a += "States opened [-]: " + solver.getStates() + "\n";
		a += "States opened per problem [-]: " + SolveProblemsFormater.statesPerProblem(solver.getStates(), solver.getCount()) + "\n";
		a += "Time per solution [us]: " + SolveProblemsFormater.getTime(solver.getTimeInUs(), solver.getCount()) + "\n";
		a += "Absolute error: " + solver.getError() + "\n";
		a += "Absolute error per item: " + SolveProblemsFormater.getError(solver.getError(), solver.getCount()) + "\n";
		a += "--------Computer readable data--------------\n";
		a += "##" + solver.getSolver().getName() + "|TIME|" + SolveProblemsFormater.getTime(solver.getTimeInUs(), solver.getCount()) + "##\n";
		a += "##" + solver.getSolver().getName() + "|STATES|" + SolveProblemsFormater.statesPerProblem(solver.getStates(), solver.getCount()) + "##\n";
		a += "--------------------------------------------\n\n";
		
		return a;
	}
	
}
