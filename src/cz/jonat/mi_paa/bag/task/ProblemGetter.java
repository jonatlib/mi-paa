package cz.jonat.mi_paa.bag.task;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import cz.jonat.mi_paa.bag.model.Problem;
import cz.jonat.mi_paa.bag.parser.EduxFileParser;
import cz.jonat.mi_paa.bag.parser.ParserInterface;

public class ProblemGetter implements Runnable {

	protected String problem;
	protected String solution; 
	
	protected ArrayList<Problem> data;
	
	@Override
	public void run() {
		try {
			ParserInterface parser = (ParserInterface) new EduxFileParser(this.problem, this.solution);
			this.data = parser.getProblem();
		} catch (FileNotFoundException e) {
			System.err.println("Could not read files.");
		}
	}
		
	public ArrayList<Problem> getData() {
		return data;
	}

	public ProblemGetter(String problem, String solution){
		this.problem = problem;
		this.solution = solution;
	}
}
