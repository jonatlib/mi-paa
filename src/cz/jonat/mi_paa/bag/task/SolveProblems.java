package cz.jonat.mi_paa.bag.task;

import java.util.ArrayList;
import java.util.Iterator;

import cz.jonat.mi_paa.bag.model.Bag;
import cz.jonat.mi_paa.bag.model.Problem;
import cz.jonat.mi_paa.bag.solver.CannotFindSolution;
import cz.jonat.mi_paa.bag.solver.SolverInterface;
import cz.jonat.mi_paa.bag.solver.SolverResult;
import cz.jonat.mi_paa.bag.watch.RunnableWatch;

public class SolveProblems {

	private class SolverWatch implements Runnable {
		protected SolverResult data;
		protected Problem p;
		protected SolverInterface solver;
			
		@Override
		public void run() {
			try {
				this.data = this.solver.getSolution(this.p);
			} catch (CannotFindSolution e) {
				this.data = null;
			}
		}
		
		public SolverResult getData(){
			return this.data;
		}
		
		public SolverWatch(SolverInterface solver, Problem p){
			this.solver = solver;
			this.p = p;
		}
	}
	
	protected ArrayList<Problem> data;
	protected SolverInterface solver;
	
	protected long time;
	protected int count;
	protected int error;
	protected int bestCost;
	protected int totalCost;
	protected int bestSolutions;
	protected int solutions;
	protected long states;
	
	protected SolverWatch getSolverWatch(Problem p){
		return new SolverWatch(this.solver, p);
	}
	
	public void solve(){
		this.solve(0);
	}
	
	@SuppressWarnings("unused")
	public void solve(int limit){
		long states = 0;
		long completeTime = 0;
		int count = 0;
		int bestSolutions = 0;
		int solutions = 0;
		int error = 0;
		int totalBestCost = 0;
		int solverBestCost = 0;
		
		for(Iterator<Problem> itt = this.data.iterator(); itt.hasNext(); ){
			if(count >= limit && limit > 0){
				break;
			}
			Problem p = itt.next();
			SolverWatch w = this.getSolverWatch(p);
			
			RunnableWatch watch = new RunnableWatch(w);
			watch.startWait();
			
			SolverResult result = w.getData();
			Bag resultBag = result.getResult();
			
			if(result == null){
				//TODO how to react ?
				System.err.println("Cannot solve problem: " + p.getId());
				continue;
			}
			
			if(p.isBestSolution(resultBag)){
				bestSolutions++;
			}
			
			if(p.isSolution(resultBag)){
				solutions++;
				completeTime += watch.getDifference();
				error += p.getAbsoluteError(resultBag);
				totalBestCost += p.getBestCost();
				solverBestCost += resultBag.getTotalCost();
				states += result.getStates();
			}
			count++;
		}
		
		this.time = completeTime;
		this.states = states;
		this.count = count;
		this.bestSolutions = bestSolutions;
		this.solutions = solutions;
		this.error = error;
		this.bestCost = totalBestCost;
		this.totalCost = solverBestCost;
	}
	
	public SolverInterface getSolver(){
		return this.solver;
	}
	
	public ArrayList<Problem> getData(){
		return this.data;
	}
	
	public long getTime(){
		return this.time;
	}
	
	public double getTimeInUs(){
		return (double) this.time / 1000.0;
	}
	
	public long getStates(){
		return this.states;
	}
	
	public double getTimeInNs(){
		return (double) this.getTimeInUs() / 1000.0;
	}
	
	public int getCount(){
		return this.count;
	}
	
	public int getBestSolutions(){
		return this.bestSolutions;
	}
	
	public int getSolutions(){
		return this.solutions;
	}
	
	public int getError(){
		return this.error;
	}
	
	public int getProblemBestCost(){
		return this.bestCost;
	}
	
	public int getSolverBestCost(){
		return this.totalCost;
	}
	
	public SolveProblems(ArrayList<Problem> data, SolverInterface solver){
		this.data = data;
		this.solver = solver;
	}
}
