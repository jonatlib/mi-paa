package cz.jonat.mi_paa.bag.parser;

import java.util.ArrayList;
import java.util.Iterator;

import cz.jonat.mi_paa.bag.model.Problem;

public interface ParserInterface extends Iterable<Problem> {
	
	public ArrayList<Problem> getProblem();

	public Iterator<Problem> iterator();
}
