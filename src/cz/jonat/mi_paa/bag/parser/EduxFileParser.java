package cz.jonat.mi_paa.bag.parser;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import cz.jonat.mi_paa.bag.model.Item;
import cz.jonat.mi_paa.bag.model.Problem;

public class EduxFileParser implements ParserInterface {

	protected BufferedReader problemReader;
	protected BufferedReader solutionReader;
	protected ArrayList<Problem> cache = null;
	
	private class CannotParseException extends Exception{
		private static final long serialVersionUID = -5409597356732946132L;
	};
	
	private class ProblemIterator implements Iterator<Problem>{
		protected ArrayList<Problem> problems;
		protected int index = 0;
		
		@Override
		public boolean hasNext() {
			return this.index < this.problems.size();
		}

		@Override
		public Problem next() {
			return this.problems.get(this.index++);
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException("Remove is not supported");			
		}
		
		public ProblemIterator(ArrayList<Problem> problems){
			this.problems = problems;
		}
	};
	
	private class ReadProblem {
		public int id;
		public int maxSize;
		public ArrayList<Item> items;
	};
	
	private class ReadSolution {
		public int id;
		public int count;
		public int bestCost;
		public boolean[] contains;
	};
	
	protected String[] prepare(String s) throws CannotParseException {
		if(s.isEmpty()){
			throw new CannotParseException();
		}
		
		String a = new String(""), b = new String(s);
		while(!b.equals(a)){
			a = new String(b);
			b = a.replaceAll("  ", " ");
		}
		
		String[] data = a.trim().split(" ");
		return data;
	}
	
	protected ReadProblem parseProblemString(String problem) throws CannotParseException {
		String[] tokens = this.prepare(problem);
		
		if(tokens.length < 3){
			throw new CannotParseException();
		}
		
		ReadProblem p = new ReadProblem();
		p.id = Integer.parseInt(tokens[0]);
		p.maxSize = Integer.parseInt(tokens[2]);
		
		int count = Integer.parseInt(tokens[1]);
		if(tokens.length - 3 < count * 2){
			throw new CannotParseException();
		}
		
		p.items = new ArrayList<Item>();
		for(int i = 3, j = 0; i < tokens.length; i+=2, j++){
			p.items.add(new Item(j, Integer.parseInt(tokens[i]), Integer.parseInt(tokens[i+1])));
		}
		
		return p;
	}
	
	protected ReadSolution parseSolutionString(String solution, ReadProblem problem) throws CannotParseException{
		if(solution.equals(new String("-"))){
			ReadSolution s = new ReadSolution();
			s.id = problem.id;
			s.count = 0;
			s.bestCost = 0;
			s.contains = null;
			return s;
		}
		
		String[] tokens = this.prepare(solution);
		
		if(tokens.length < 3){
			throw new CannotParseException();
		}
		ReadSolution s = new ReadSolution();
		s.id = Integer.parseInt(tokens[0]);
		s.count = Integer.parseInt(tokens[1]);
		s.bestCost = Integer.parseInt(tokens[2]);
		
		if(tokens.length - 3 < s.count){
			throw new CannotParseException();
		}
		
		s.contains = new boolean[s.count];
		for(int i = 3, j = 0; i < tokens.length; i++, j++){
			s.contains[j] = (( Integer.parseInt(tokens[i]) == 1 ) ? true : false);
		}
		
		return s;
	}
	
	@Override
	public ArrayList<Problem> getProblem() {
		if(this.cache != null){
			return this.cache;
		}
		
		ArrayList<Problem> list = new ArrayList<Problem>();
		
		try {
			while(this.problemReader.ready() && (this.solutionReader == null || this.solutionReader.ready()) ){
				String problem  = this.problemReader.readLine();
				
				String solution = null;
				if(this.solutionReader == null){
					solution = "-";
				}else{
					solution = this.solutionReader.readLine();
				}
				
				
				try{
					ReadProblem  p = this.parseProblemString(problem);
					ReadSolution s = this.parseSolutionString(solution, p);
				
					if(p.id != s.id){
						//TODO throw exception
						System.err.println("Error ids do not match !");
					}
					
					Problem problemInstance = new Problem(p.id, p.maxSize, s.bestCost);
					for(Item i : p.items){
						problemInstance.add(i);
					}
					for(int i = 0; i < s.count; i++){
						if(s.contains[i]){
							problemInstance.addSolution(problemInstance.getItemsOrdered().get(i));
						}
					}
					list.add(problemInstance);
				
				}catch(CannotParseException e){
					continue;
				}
			}
		} catch (IOException e) {
			//TODO throw exception
			System.err.println("Files not readable !");
		}
		
		this.cache = list;
		return list;
	}
	
	public Iterator<Problem> iterator(){
		return (Iterator<Problem>) new ProblemIterator(this.getProblem()); 
	}
	
	public EduxFileParser(String problem, String solution) throws FileNotFoundException{
		this.problemReader  = new BufferedReader(new FileReader(problem));
		if(solution.equals(new String("-"))){
			this.solutionReader = null;
		}else{
			this.solutionReader = new BufferedReader(new FileReader(solution));
		}
	}

}
