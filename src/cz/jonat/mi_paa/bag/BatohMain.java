package cz.jonat.mi_paa.bag;

import cz.jonat.mi_paa.bag.formater.DiffSolverProblemsFormater;
import cz.jonat.mi_paa.bag.formater.RunnableWatchFormater;
import cz.jonat.mi_paa.bag.formater.SolveProblemsFormater;
import cz.jonat.mi_paa.bag.solver.BranchBound;
import cz.jonat.mi_paa.bag.solver.DumpSolver;
import cz.jonat.mi_paa.bag.solver.EthalonSolver;
import cz.jonat.mi_paa.bag.solver.ExpanseOverWeight;
import cz.jonat.mi_paa.bag.solver.ForceSolver;
import cz.jonat.mi_paa.bag.solver.Fptas;
import cz.jonat.mi_paa.bag.solver.HeuristicSolver;
import cz.jonat.mi_paa.bag.solver.SimulatedAnnealingSolver;
import cz.jonat.mi_paa.bag.solver.SolverInterface;
import cz.jonat.mi_paa.bag.task.ProblemGetter;
import cz.jonat.mi_paa.bag.task.SolveProblems;
import cz.jonat.mi_paa.bag.watch.RunnableWatch;

public class BatohMain {

	protected static String problemFile;
	protected static String solutionFile;
	protected static boolean force = false;
	protected static int limit = 0;
	protected static String solver = null;
	
	protected static int prepareArgs(String[] args){
		if(args.length < 2){
			return 1;
		}
		
		BatohMain.problemFile  = args[0];
		BatohMain.solutionFile = args[1];
		if(args.length > 2){
			int force = Integer.parseInt(args[2]);
			BatohMain.force = (force > 0) ? true : false;
		}
		
		if(args.length > 3){
			BatohMain.limit = Integer.parseInt(args[3]);
		}
		
		if(args.length > 4){
			BatohMain.solver = args[4];
		}
		
		return 0;
	}
	
	protected static SolveProblems solver(ProblemGetter getter, SolverInterface solver){
		SolveProblems solverhelper = new SolveProblems(getter.getData(), solver);
		solverhelper.solve(BatohMain.limit);
		System.out.print(SolveProblemsFormater.format(solverhelper));
		return solverhelper;
	}
	
	protected static void dumpsolver(ProblemGetter getter){
		SolveProblems solverhelper = new SolveProblems(getter.getData(), new DumpSolver());
		for(int i = 0; i < 20; i++){
			solverhelper.solve();
		}
//		System.out.print(SolveProblemsFormater.format(solverhelper));
	}
	
	public static void main(String[] args) {
		int result = BatohMain.prepareArgs(args);
		if( result != 0 ){
			System.exit(result);
		}
		
		System.out.println("Starting:");
		
		ProblemGetter getter = new ProblemGetter(BatohMain.problemFile, BatohMain.solutionFile);
		RunnableWatch loadProblems = new RunnableWatch(getter);
		loadProblems.startWait();
		
		System.out.print(RunnableWatchFormater.format("Loading problems", loadProblems));
		if(getter.getData() == null){
			System.exit(1);
		}
		
		BatohMain.dumpsolver(getter);
		SolveProblems sEthalon = BatohMain.solver(getter, new EthalonSolver());
		
		if (BatohMain.force){
			SolveProblems s = BatohMain.solver(getter, new ForceSolver());
			if(BatohMain.solutionFile.equals(new String("-"))){
				sEthalon = s;
			}
			System.out.print(DiffSolverProblemsFormater.format(sEthalon, s));
		}
		
		if(BatohMain.solver != null){
			
			{
				SolveProblems s = BatohMain.solver(getter, new SimulatedAnnealingSolver(1, 1));
				System.out.print(DiffSolverProblemsFormater.format(sEthalon, s));
			}
			
			for(int i = 0; i < 16; i++){
				SolveProblems s = BatohMain.solver(getter, new SimulatedAnnealingSolver(i * 10, 1));
				System.out.print(DiffSolverProblemsFormater.format(sEthalon, s));
			}
			
			for(int i = 0; i < 16; i++){
				SolveProblems s = BatohMain.solver(getter, new SimulatedAnnealingSolver(1, i * 100));
				System.out.print(DiffSolverProblemsFormater.format(sEthalon, s));
			}
			
		}else{
		
			{
				SolveProblems s = BatohMain.solver(getter, new HeuristicSolver());
				System.out.print(DiffSolverProblemsFormater.format(sEthalon, s));
			}
			
			{
				SolveProblems s = BatohMain.solver(getter, new BranchBound());
				System.out.print(DiffSolverProblemsFormater.format(sEthalon, s));
			}
			
			{
				SolveProblems s = BatohMain.solver(getter, new Fptas(0.5f));
				System.out.print(DiffSolverProblemsFormater.format(sEthalon, s));
			}
			
	//		{
	//			SolveProblems s = BatohMain.solver(getter, new Fptas(0.2f));
	//			System.out.print(DiffSolverProblemsFormater.format(sEthalon, s));
	//		}
			
			{
				SolveProblems s = BatohMain.solver(getter, new ExpanseOverWeight());
				System.out.print(DiffSolverProblemsFormater.format(sEthalon, s));
			}
		
		}
	}

}
