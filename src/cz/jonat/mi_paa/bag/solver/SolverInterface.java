package cz.jonat.mi_paa.bag.solver;

import cz.jonat.mi_paa.bag.model.Problem;

public interface SolverInterface {
	
	public SolverResult getSolution(Problem problem) throws CannotFindSolution;
	
	public String getName();
	
	public String getDescription();
}
