package cz.jonat.mi_paa.bag.solver;

import cz.jonat.mi_paa.bag.model.Bag;
import cz.jonat.mi_paa.bag.model.Item;
import cz.jonat.mi_paa.bag.model.Problem;

public class EthalonSolver implements SolverInterface {
	
	public String getName(){
		return "Ethalon";
	}
	
	public String getDescription(){
		return "Always return best solution for comparsion";
	}
	
	@Override
	public SolverResult getSolution(Problem problem) throws CannotFindSolution {
		Bag b = problem.getEmptyBag();
		long states = 0;
		for(Item i : problem.getSolution()){
			b.add(i);
			states++;
		}
		
		return new SolverResult(b, states);
	}
}
