package cz.jonat.mi_paa.bag.solver;

import cz.jonat.mi_paa.bag.model.Bag;
import cz.jonat.mi_paa.bag.model.Problem;

public class DumpSolver implements SolverInterface {
	
	public String getName(){
		return "Dump";
	}
	
	public String getDescription(){
		return "Used only for warmup the virtual machine.";
	}
	
	@Override
	public SolverResult getSolution(Problem problem) throws CannotFindSolution {
		Bag b = problem.getEmptyBag();
		b.add(problem.getItemsOrdered().get(0));
		
		return new SolverResult(b, 1);
	}
}
