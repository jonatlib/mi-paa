package cz.jonat.mi_paa.bag.solver;

import java.util.ArrayList;

import cz.jonat.mi_paa.bag.model.Bag;
import cz.jonat.mi_paa.bag.model.Item;
import cz.jonat.mi_paa.bag.model.Problem;


public class BranchBound implements SolverInterface {
	
	private Bag bag;
	private Problem tested;
	private int solutionCost = 0;
	private boolean solution[];
	
	protected long states;
	
	private void init(Problem tested){
		this.tested = tested;
		this.bag = tested.getEmptyBag();
		this.solutionCost = 0;
		this.solution = new boolean[tested.getItemsOrdered().size()];
	}
	
	private int getCostBoundary(Item i){
		return i.getBoundaryCost();
	}
	
	private void saveSolution(ArrayList<Item> items, int cost){
		this.solutionCost = cost;
		for(int i = 0; i < this.solution.length; i++){
			if(items.get(i).getIncluded() == Item.Type.INCLUDE){
				this.solution[i] = true;
			}else{
				this.solution[i] = false;
			}
		}
	}
	
	private void branchAndBound(ArrayList<Item> items, int depth, int weight, int value) {
		this.states++;
		if(this.solutionCost < value){
			this.saveSolution(items, value);
		}
		if(depth < this.tested.getItemsOrdered().size() ){
			if( weight + items.get(depth).getWeight() <= bag.getMaxWeight() ){
				if( items.get(depth).getCost() + this.getCostBoundary(items.get(depth)) + value > this.solutionCost ){
					items.get(depth).setIncluded(Item.Type.INCLUDE);
					this.branchAndBound(items, depth + 1, weight + items.get(depth).getWeight(), value+items.get(depth).getCost());
				}
			}
			if( this.getCostBoundary(items.get(depth)) + value > this.solutionCost ){
				items.get(depth).setIncluded(Item.Type.NOT_INCLUDED);
				this.branchAndBound(items, depth + 1, weight, value);
			}
		}
	}
	
	@Override
	public SolverResult getSolution(Problem problem) throws CannotFindSolution {
		this.states = 0;
		Bag bag = problem.getEmptyBag();
		
		ArrayList<Item> newData = new ArrayList<Item>( problem.getItemsOrdered() );
		for(int i = 0; i < newData.size(); i++){
			newData.get(i).setIncluded(Item.Type.UNKOWN);
		}
		newData.get(newData.size() - 1).setBoundaryCost(0);
		newData.get(newData.size() - 2).setBoundaryCost(newData.get(newData.size() - 1).getCost());
		for (int i = newData.size() - 3; i >= 0; i--) {
			newData.get(i).setBoundaryCost(newData.get(i+1).getBoundaryCost() + newData.get(i+1).getCost());
		}
		this.init( problem );
		
		this.branchAndBound(newData, 0, 0, 0);
		for(int i = 0; i < this.solution.length; i++){
			if(this.solution[i]){
				Item item = problem.getItemsOrdered().get(i);
				item.setIncluded(Item.Type.INCLUDE);
				bag.add(item);
			}
		}
		return new SolverResult(bag, this.states);
	}

	@Override
	public String getName() {
		return "branch & bound";
	}

	@Override
	public String getDescription() {
		return "branch & bound";
	}

}
