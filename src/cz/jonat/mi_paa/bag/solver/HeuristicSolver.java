package cz.jonat.mi_paa.bag.solver;

import java.util.PriorityQueue;

import cz.jonat.mi_paa.bag.model.Bag;
import cz.jonat.mi_paa.bag.model.Item;
import cz.jonat.mi_paa.bag.model.Problem;

public class HeuristicSolver implements SolverInterface {

	public String getName(){
		return "Heuristic";
	}
	
	public String getDescription(){
		return "Bruteforce solver using BFS and weight / price heurestics.";
	}
	
	@Override
	public SolverResult getSolution(Problem problem) throws CannotFindSolution {
		Bag b = problem.getEmptyBag();
		long states = 0;
		
		PriorityQueue<Item> items = new PriorityQueue<Item>();
		for(Item i : problem.getItemsOrdered()){
			items.add(i);
		}
		while( !items.isEmpty() ){
			Item i = items.poll();
			states++;
			
			b.add(i);
			if(b.isOverWeighted()){
				b.remove(i);
			}
		}		
		
		return new SolverResult(b, states);
	}

}
