package cz.jonat.mi_paa.bag.solver;

import java.util.ArrayList;

import cz.jonat.mi_paa.bag.model.Bag;
import cz.jonat.mi_paa.bag.model.Item;
import cz.jonat.mi_paa.bag.model.Problem;

public class ExpanseOverWeight implements SolverInterface {

	protected long states;
	
	private class Vector {
		public int itemIndex = 0;
		public int maxWeigt = 0;
		public int currentWeight = 0;
		public int currentCost = 0;
		public boolean notUsed = false;
		public ArrayList<Integer> usedIndexes = new ArrayList<Integer>();
		
		Vector(int itemIndex, int maxWeight, int currentWeight, int currentCost){
			this.itemIndex = itemIndex;
			this.maxWeigt = maxWeight;
			this.currentWeight = currentWeight;
			this.currentCost = currentCost;
		};
		
		Vector(int itemIndex, int maxWeight){
			this(itemIndex, maxWeight, 0, 0);
		};
		
		Vector(boolean notUsed){
			this.notUsed = notUsed;
		}
	};
	
	protected boolean isTrivial(Vector v){
		return v.itemIndex < 0;
	}
	
	protected Vector setTrivial(Vector v){
		v.currentCost = 0;
		v.currentWeight = 0;
		return v;
	}
	
	protected Vector addItem(int index, Item i, Vector v, int maxWeight){
		if(v.notUsed) return v;
		int newWeight = v.currentWeight + i.getWeight();
		int newCost = v.currentCost + i.getCost();
		if(newWeight <= maxWeight){
			v.currentWeight = newWeight;
			v.currentCost = newCost;
			v.usedIndexes.add(index);
		}
		return v;
		
	}
	
	protected Vector expanse(Vector v, ArrayList<Item> items){
		if(v.maxWeigt < 0 || v.notUsed) return new Vector(true);
		if(this.isTrivial(v)) return this.setTrivial(v);
		
		this.states++;
		
		int index = v.itemIndex;
		Vector v1 = this.expanse(new Vector(index - 1, v.maxWeigt), items);
		Vector v2 = this.expanse(new Vector(index - 1, v.maxWeigt - items.get(index).getWeight()), items);
		
		v1 = this.addItem(index, items.get(index), v1, v.maxWeigt);
		v2 = this.addItem(index, items.get(index), v2, v.maxWeigt);
		
		if( v1.currentCost > v2.currentCost || v2.notUsed){
			return v1;
		}
		return v2;
	}
	
	@Override
	public SolverResult getSolution(Problem problem) throws CannotFindSolution {
		this.states = 0;
		ArrayList<Item> items = problem.getItemsOrdered();
		Bag bag = problem.getEmptyBag();
		
		Vector begin = new Vector(items.size() - 1, bag.getMaxWeight());
		Vector solution = this.expanse(begin, items);
		
		for(int presented : solution.usedIndexes){
			bag.add( items.get(presented) );
		}
		return new SolverResult(bag, this.states);
	}

	@Override
	public String getName() {
		return "ExpanseOverWeight";
	}

	@Override
	public String getDescription() {
		return "Solver expance over weight";
	}

}
