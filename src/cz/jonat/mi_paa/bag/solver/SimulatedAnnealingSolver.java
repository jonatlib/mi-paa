package cz.jonat.mi_paa.bag.solver;

import java.util.ArrayList;
import java.util.Random;

import cz.jonat.mi_paa.bag.model.Bag;
import cz.jonat.mi_paa.bag.model.Item;
import cz.jonat.mi_paa.bag.model.Problem;

public class SimulatedAnnealingSolver implements SolverInterface {

	protected Random r;
	
	protected int temp;
	
	protected float factor;
	
	public SimulatedAnnealingSolver(int temp, float factor){
		this.temp = temp;
		this.factor = factor;
	}
	
	public String getName(){
		return "SimulatedAnnealing";
	}
	
	public String getDescription(){
		return "Solving using SimulatedAnnealing";
	}
	
	@Override
	public SolverResult getSolution(Problem problem) throws CannotFindSolution {
		this.r = new Random(problem.getBestCost() + problem.getId() * 1000);
		Bag b = problem.getEmptyBag();
		long states = 0;
		
		ArrayList<Item> items = problem.getItemsOrdered();
		boolean[] configuration = this.initConfiguration(new boolean[items.size()]);
		
		boolean[] bestConfiguration = this.initConfiguration(new boolean[items.size()]);
		int bestCost = 0;
		
		double minTemperature = this.initFreezeTemperature(configuration);
		int maxWeight = b.getMaxWeight();
		int posibleNewWeight = 0;
		
		double initialTemperature = this.initTemperature(configuration);
		double temperature = this.initTemperature(configuration);
		int currentCost = this.getCost(items, configuration, -1);
		int position = getNeighbourIndex(temperature, initialTemperature, 0, configuration.length);
		
		
		while(temperature > minTemperature){
			position  = this.getNeighbourIndex(temperature, initialTemperature, position, configuration.length);
			
			currentCost = this.newState(position, currentCost, temperature, initialTemperature, maxWeight, configuration, items);
			
			posibleNewWeight = this.getWeight(items, configuration, -1);
			if(posibleNewWeight <= maxWeight && currentCost > bestCost){
				bestCost = currentCost;
				bestConfiguration = this.cloneConfiguration(configuration);
			}
			
			temperature = this.coolDown(temperature, states);
			++states;
		}
		
		for(int i = 0; i < bestConfiguration.length; i++){
			if(bestConfiguration[i]){
				b.add(items.get(i));
			}
		}
		
		return new SolverResult(b, states);
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	protected double initTemperature(boolean[] configuration){
		return configuration.length * this.temp;
//		return Math.pow(3, 5);
//		return Math.pow(configuration.length, 10);
//		return configuration.length * 10.0;
	}
	
	protected double getAnnealingFactor(long states){
		return 1.0 - ( Math.pow(10.0, -5) * this.factor );
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	protected int getNeighbourIndex(double temperature, double initialTemperature, int current, int max){
		return this.r.nextInt(max);
//		double d = r.nextDouble();
//		double temp = (temperature / initialTemperature) + 0.4;
//		
//		int range = (int) Math.ceil(temp * (double) max);
//		int position = current + ( (int) ((d - 0.5) * (double) range) );
//		
//		return Math.abs(position) % max;
	}
	
	protected double coolDown(double temperature, long states){
		return temperature * this.getAnnealingFactor(states);
	}
	
	protected double initFreezeTemperature(boolean[] configuration){
		return 1.0;
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	protected int newState(
			int position,
			int currentCost,
			
			double temperature,
			double initialTemperature,
			
			int maxWeight,
			
			boolean[] configuration,
			ArrayList<Item> items
			){
		int newCost   = this.getCost(items, configuration, position);
		int newWeight = this.getWeight(items, configuration, position);
		int curentWeight = this.getWeight(items, configuration, -1);
		
		double cos = 1.0 - ((Math.cos((1.0 - ( temperature / initialTemperature )) * Math.PI) * 0.5) + 0.5);
		
		boolean canDoAnything = ( this.r.nextDouble() >= cos );
		
		if(canDoAnything){
			return this.changeItemAndComputeCost(newCost, position, configuration);
		}
		
		if(newCost >= currentCost && newWeight <= maxWeight){
			return this.changeItemAndComputeCost(newCost, position, configuration);
		}
		
		if(curentWeight > maxWeight && newWeight < curentWeight){
			return this.changeItemAndComputeCost(newCost, position, configuration);
		}
		
		return currentCost;
	}
	
	protected int changeItemAndComputeCost(int newCost, int position, boolean[] configuration){
		this.negation(configuration, position);
		return newCost;
	}
	
	protected void negation(boolean[] configuration, int position){
		configuration[position] = !configuration[position];
	}
	
	protected int getCost(ArrayList<Item> items, boolean[] configuration, int negation) {
		int result = 0;
		for(int i = 0; i < configuration.length; i++){
			if(negation > -1 && i == negation){
				if(!configuration[i]){
					result += items.get(i).getCost();
				}
			}else{
				if(configuration[i]){
					result += items.get(i).getCost();
				}
			}
		}
		return result;
	}
	
	protected int getWeight(ArrayList<Item> items, boolean[] configuration, int negation) {
		int result = 0;
		for(int i = 0; i < configuration.length; i++){
			if(negation > -1 && i == negation){
				if(!configuration[i]){
					result += items.get(i).getWeight();
				}
			}else{
				if(configuration[i]){
					result += items.get(i).getWeight();
				}
			}
		}
		return result;
	}

	protected boolean[] initConfiguration(boolean[] configuration){
		for(int i = 0; i < configuration.length; i++){
			configuration[i] = true;
		}
		return configuration;
	}
	
	protected boolean[] cloneConfiguration(boolean[] configuration){
		boolean[] newConfiguration = new boolean[configuration.length];
		for(int i = 0; i < configuration.length; i++){
			newConfiguration[i] = configuration[i];
		}
		return newConfiguration;
	}
	
}
