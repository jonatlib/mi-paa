package cz.jonat.mi_paa.bag.solver;

import java.util.ArrayList;
import java.util.Iterator;

import cz.jonat.mi_paa.bag.model.Bag;
import cz.jonat.mi_paa.bag.model.Item;
import cz.jonat.mi_paa.bag.model.Problem;

public class Fptas implements SolverInterface {
	
	private float epsilon;
	protected long states;
	
	private class Vector {
		public int itemIndex = 0;
		public int maxCost = 0;
		public int maxWeight = 0;
		public int currentWeight = 0;
		public int currentCost = 0;
		public boolean notUsed = false;
		public ArrayList<Integer> usedIndexes = new ArrayList<Integer>();
		
		Vector(int itemIndex, int maxCost, int maxWeight, int currentWeight, int currentCost){
			this.itemIndex = itemIndex;
			this.maxCost = maxCost;
			this.maxWeight = maxWeight;
			this.currentWeight = currentWeight;
			this.currentCost = currentCost;
		};
		
		Vector(int itemIndex, int maxCost, int maxWeight){
			this(itemIndex, maxCost, maxWeight, 0, 0);
		};
		
		Vector(boolean notUsed){
			this.notUsed = notUsed;
		}
	};
	
	protected boolean isTrivial(Vector v){
		return v.itemIndex < 0;
	}
	
	protected Vector setTrivial(Vector v){
		v.currentCost = 0;
		v.currentWeight = 0;
		return v;
	}
	
	protected Vector addItem(int index, Item i, Vector v, int maxCost){
		if(v.notUsed) return v;
		int newWeight = v.currentWeight + i.getWeight();
		int newCost = v.currentCost + i.getCategoryCost();
		if(newCost <= maxCost && newWeight <= v.maxWeight){
			v.currentWeight = newWeight;
			v.currentCost = newCost;
			v.usedIndexes.add(index);
		}
		return v;
		
	}
	
	protected Vector expanse(Vector v, ArrayList<Item> items){
		if(v.maxCost < 0 || v.notUsed) return new Vector(true);
		if(this.isTrivial(v)) return this.setTrivial(v);
		
		this.states++;
		
		int index = v.itemIndex;
		Vector v1 = this.expanse(new Vector(index - 1, v.maxCost, v.maxWeight), items);
		Vector v2 = this.expanse(new Vector(index - 1, v.maxCost - items.get(index).getCategoryCost(), v.maxWeight), items);
		
		v1 = this.addItem(index, items.get(index), v1, v.maxCost);
		v2 = this.addItem(index, items.get(index), v2, v.maxCost);
		
		if( v1.currentCost > v2.currentCost || v2.notUsed){
			return v1;
		}
		return v2;
	}
	
	@Override
	public SolverResult getSolution(Problem problem) throws CannotFindSolution {
		this.states = 0;
		ArrayList<Item> items = problem.getItemsOrdered();
		Bag bag = problem.getEmptyBag();
		
		int costSum = 0;
		for(Iterator<Item> itt = items.iterator(); itt.hasNext(); ){
			Item i = itt.next();
			if(i.getWeight() > bag.getMaxWeight()){
				items.remove(i);
				continue;
			}
			costSum += i.getCost();
		}
		
		int numsOfBitExclude = this.getBits(this.epsilon, costSum, items);
		for(Iterator<Item> itt = items.iterator(); itt.hasNext(); ){
			Item i = itt.next();
			i.setCategoryCost( ( i.getCost() >> numsOfBitExclude ) );
		}
		
		int costCategSum = 0;
		for(Iterator<Item> itt = items.iterator(); itt.hasNext(); ){
			Item i = itt.next();
			costCategSum += i.getCategoryCost();
		}
		
		Vector begin = new Vector(items.size() - 1, costCategSum, bag.getMaxWeight());
		Vector solution = this.expanse(begin, items);
		
		for(int presented : solution.usedIndexes){
			bag.add( items.get(presented) );
		}
		return new SolverResult(bag, this.states);
	}

	private int getBits(float epsilon, int costSum, ArrayList<Item> items){
		int b = 0;
		if (epsilon > 0) {
			b = (int)Math.ceil(log2(((float) costSum) / (float) items.size()));
			b = (int) ((epsilon) * (float) b);
		}
		return b;
	}
	
	private float log2(float n){
	    return (float) (Math.log((double)n) / Math.log(2));
	}
	
	@Override
	public String getName() {
		return "FPTAS E:(" + this.epsilon + ")";
	}

	@Override
	public String getDescription() {
		return "FPTAS with epsilon:" + this.epsilon;
	}

	public Fptas(float epsilon){
		this.epsilon = epsilon;
	}
}
