package cz.jonat.mi_paa.bag.solver;

import java.util.ArrayList;

import cz.jonat.mi_paa.bag.model.Bag;
import cz.jonat.mi_paa.bag.model.Item;
import cz.jonat.mi_paa.bag.model.Configuration;
import cz.jonat.mi_paa.bag.model.Problem;

public class ForceSolver implements SolverInterface {
	
	public String getName(){
		return "Force";
	}
	
	public String getDescription(){
		return "Bruteforce solver using BFS";
	}
	
	@Override
	public SolverResult getSolution(Problem problem) throws CannotFindSolution {
		Configuration config = problem.getConfiguration();
		Bag best = problem.getEmptyBag();
		long states = 0;
		
		for(;config.hasNext();){
			ArrayList<Item> items = problem.getItemsByConfiguration(config);
			Bag tmp = problem.getEmptyBag();
			tmp.add(items);
			states++;
			
			if(best.getTotalCost() < tmp.getTotalCost() && !tmp.isOverWeighted()){
				best = tmp;
			}
			
			config.next();
		}
		
		problem.setBestCost(best.getTotalCost());
		
		return new SolverResult(best, states);
	}
}
