package cz.jonat.mi_paa.bag.solver;

import cz.jonat.mi_paa.bag.model.Bag;

/**
 * @author jonatlib
 *
 */
public class SolverResult {
	protected Bag result;
	protected long states;
	
	public Bag getResult() {
		return result;
	}

	public long getStates() {
		return states;
	}

	public SolverResult(Bag result, long states) {
		this.result = result;
		this.states = states;
	}
}
