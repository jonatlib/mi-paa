package cz.jonat.mi_paa.bag.model;

import java.util.ArrayList;
import java.util.Iterator;

public class Bag {
	
	protected int maxWeight = 0;
	protected int weight 	= 0;
	protected int cost 		= 0;
	
	protected ArrayList<Item> content;
	
	public int getMaxWeight(){
		return this.maxWeight;
	}
	
	public void add(ArrayList<Item> items){
		for(Item i : items){
			this.add(i);
		}
	}
	
	public void add (Item i){
		if(this.content.add(i) && i.getIncluded() == Item.Type.INCLUDE){
			this.weight += i.getWeight();
			this.cost += i.getCost();
		}
	}
	
	public void remove(Item i){
		if(this.content.remove(i)){
			this.weight -= i.getWeight();
			this.cost -= i.getCost();
		}
	}
	
	public void removeLast(){
		if(this.content.size() > 0){
			Item i = this.content.get( this.content.size() - 1 );
			this.remove(i);
		}
	}
	
	public boolean contains(Item i){
		return this.content.contains(i);
	}
	
	public boolean isOverWeighted(){
		return (this.maxWeight < this.weight);
	}
	
	public int getNumberOfItems(){
		return this.content.size();
	}
	
	public int getTotalWeight(){
		return this.weight;
	}
	
	public int getTotalCost(){
		return this.cost;
	}
	
	public Iterator<Item> itterator(){
		return this.content.iterator();
	}
	
	@Override
	public String toString() {
		String a = "Bag: W:" + this.weight + " C:" + this.cost + "{";
		a += "\n";
		for(Item i : this.content){
			a += "\t" + i.toString();
		}
		a += "}";
		return a;
	}
	
	public Bag(int maxWeight){
		this.maxWeight = maxWeight;
		this.content = new ArrayList<Item>(40);
	}
	
}
