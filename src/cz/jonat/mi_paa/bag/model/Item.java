package cz.jonat.mi_paa.bag.model;

import java.util.Comparator;

public class Item implements Comparable<Item>, Comparator<Item> {

	public enum Type{
		NOT_INCLUDED, INCLUDE, UNKOWN;
	};
	
	protected int order;
	
	protected int weight;
	
	protected int cost;
	
	protected int categoryCost;
	
	protected int boundaryCost;
	
	protected Type included;

	public int getWeight() {
		return weight;
	}

	public int getCost() {
		return cost;
	}
	
	public int getOrder(){
		return this.order;
	}
	
	public Type getIncluded(){
		return this.included;
	}
	
	public void setIncluded(Type included){
		this.included = included;
	}
	
	public float getIndex(){
		return (float) this.getCost() / (float) this.getWeight();
	}
	
	public int getCategoryCost() {
		return categoryCost;
	}

	public void setCategoryCost(int categoryCost) {
		this.categoryCost = categoryCost;
	}

	public int getBoundaryCost() {
		return boundaryCost;
	}

	public void setBoundaryCost(int boundaryCost) {
		this.boundaryCost = boundaryCost;
	}

	public Item(int order, int weight, int cost){
		this.order = order;
		this.weight = weight;
		this.cost   = cost;
		this.included = Item.Type.INCLUDE;
	}

	@Override
	public String toString() {
		return "Item [#" + this.order + "]: C:" + this.cost + " W:" + this.weight + ";";
	}
	
	@Override
	public int compare(Item o1, Item o2) {
		float i = o1.getIndex() - o2.getIndex();
		return ( (i == 0) ? 0 : ( (i > 0) ? -1 : 1 ) );
	}

	@Override
	public int compareTo(Item o) {
		return this.compare(this, o);
	}
	
}
