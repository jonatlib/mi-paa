package cz.jonat.mi_paa.bag.model;

import java.util.ArrayList;
import java.util.Iterator;

public class Problem {
	protected int id;
	protected ArrayList<Item> ordered;
	protected ArrayList<Item> solution;
	
	protected int maxWeight;
	protected int bestCost;
	
	public void add(Item i){
		this.ordered.add(i);
	}
	
	public void addSolution(Item i){
		this.solution.add(i);
	}
	
	public ArrayList<Item> getItemsOrdered(){
		return this.ordered;
	}
	
	public ArrayList<Item> getSolution() {
		return this.solution;
	}
	
	public Bag getEmptyBag(){
		return new Bag(this.maxWeight);
	}
	
	public int getId(){
		return this.id;
	}
	
	public int getBestCost(){
		return this.bestCost;
	}
	
	public void setBestCost(int bestCost){
		this.bestCost = bestCost;
	}
	
	public int getAbsoluteError(Bag b){
		int e = this.bestCost - b.getTotalCost();
		return (e < 0) ? -e : e;
	}
	
	public boolean isBestSolution(Bag b){
		if(this.solution.size() != b.getNumberOfItems()) return false;
		if(b.getTotalCost() >= this.bestCost && !b.isOverWeighted()){
			return true;
		}
		for(Iterator<Item> itt = this.solution.iterator(); itt.hasNext(); ){
			Item i = itt.next();
			if(!b.contains(i)){
				return false;
			}
		}
		return true;
	}
	
	public boolean isSolution(Bag b){
		return !b.isOverWeighted();
	}
	
	public Configuration getConfiguration(){
		return new Configuration(this.ordered.size());
	}
	
	public Item getItem(int i){
		return this.ordered.get(i);
	}
	
	public ArrayList<Item> getItemsByConfiguration(Configuration c){
		ArrayList<Item> items = new ArrayList<Item>();
		for(int i = 0; i < c.getConfiguration().length; i++){
			if(c.getConfiguration()[i]){
				items.add(this.getItem(i));
			}
		}
		return items;
	}
	
	public Problem(int id, int maxSize, int bestCost){
		this.id = id;
		this.maxWeight = maxSize;
		this.bestCost = bestCost;
		
		this.ordered = new ArrayList<Item>();
		this.solution = new ArrayList<Item>();
	}
}
