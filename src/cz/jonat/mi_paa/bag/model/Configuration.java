package cz.jonat.mi_paa.bag.model;

import java.util.Iterator;

public class Configuration implements Iterator<Configuration> {

	protected boolean gotLast = false;
	protected int totalCount;
	protected boolean[] configuration;
	
	public int getConfigurationCount(){
		int count = 0;
		for(int i = 0; i < this.configuration.length; i++){
			if(this.configuration[i]){
				count++;
			}
		}
		return count;
	}
	
	public boolean[] getConfiguration(){
		return this.configuration;
	}
	
	public Configuration(int count){
		this.totalCount = count;
		this.configuration = new boolean[count];
		for(int i = 0; i < this.configuration.length; i++){
			this.configuration[i] = false;
		}
	}
	
	public Configuration(boolean[] items){
		this.totalCount = items.length;
		this.configuration = items;
	}

	@Override
	public boolean hasNext() {
		for(int i = 0; i < this.configuration.length; i++){
			if(!this.configuration[i]){
				return true;
			}
		}
		if(this.gotLast){
			return false;
		}
		this.gotLast = true;
		return true;
	}

	@Override
	public Configuration next() {
		boolean add = true;
		for (int i = 0; i < this.configuration.length; i++){
			if(add){
				if(this.configuration[i]){
					this.configuration[i] = false;
					add = true;
				}else{
					this.configuration[i] = true;
					add = false;
				}
			}else{
				break;
			}
		}
		return this;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException("Remove is not supported");
	}
	
}
