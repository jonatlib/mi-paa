package cz.jonat.mi_paa.bag.watch;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;

public class RunnableWatch {

	private class WatchThread extends Thread {
		protected Runnable run;
		protected RunnableWatch watch;
		
		public void run() {
			try{
				this.watch.startWatch();
				this.run.run();
			} finally {
				this.watch.stopWatch();
			}
		}
		public WatchThread(Runnable r, RunnableWatch w){
			this.run = r;
			this.watch = w;
		}
	}
	
	protected WatchThread thr;
	protected ThreadMXBean bean;
	
	protected long start;
	protected long stop;
	
	public void start(){
		this.thr.start();
	}
	
	public void join(){
		try {
			this.thr.join();
		} catch (InterruptedException e) {
			System.err.println("Thread interupted !");
		}
	}
	
	public void startWait(){
		this.start();
		this.join();
	}
	
	public void startWatch(){
		this.start = bean.getThreadCpuTime(this.thr.getId());
	}
	
	public void stopWatch(){
		this.stop = bean.getThreadCpuTime(this.thr.getId());
	}
	
	public long getStart(){
		return this.start;
	}
	
	public long getStop(){
		return this.stop;
	}
	
	public long getDifference(){
		return this.getStop() - this.getStart();
	}
	
	public double getTimeInUsec(){
		return ((double) this.getDifference() / 1000.0);
	}
	
	public double getTimeInmsec(){
		return this.getTimeInUsec() / 1000.0;
	}
	
	public RunnableWatch(Runnable run){
		this.thr = new WatchThread(run, this);
		this.bean = ManagementFactory.getThreadMXBean();
	}
	
}
